use raui_core::prelude::*;

widget_component! {
    pub images(key) {
        widget!{
            (#{key} horizontal_box [
                (image_back)
                (image_color_1)
                (image_color_2)
            ])
        }
    }
}

widget_component! {
    image_back(key) {
        widget!{(#{"back"} image_box: {ImageBoxProps {
            height: ImageBoxSizeValue::Exact(300.0),
            width: ImageBoxSizeValue::Exact(300.0),
            material: ImageBoxMaterial::Image(ImageBoxImage {
                id: "back".to_owned(),
                ..Default::default()
            }),
            ..Default::default()
        } })}
    }
}

widget_component! {
    image_color_1(key) {
        widget!{(#{"color1"} image_box: {ImageBoxProps {
            height: ImageBoxSizeValue::Exact(300.0),
            width: ImageBoxSizeValue::Exact(300.0),
            material: ImageBoxMaterial::Color(Color { r: 0.6, g: 0.8, b: 0.2, a: 0.75 }),
            ..Default::default()
        } })}
    }
}

widget_component! {
    image_color_2(key) {
        widget!{(#{"color2"} image_box: {ImageBoxProps {
            height: ImageBoxSizeValue::Exact(300.0),
            width: ImageBoxSizeValue::Exact(300.0),
            material: ImageBoxMaterial::Color(Color { r: 0.2, g: 0.6, b: 0.8, a: 0.75 }),
            ..Default::default()
        } })}
    }
}
