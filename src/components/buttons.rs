use raui_core::prelude::*;

use crate::components::tab_button::{tab_button, TabButtonProps};

widget_component! {
    pub buttons(key) {
        widget!{
            (#{key} horizontal_box [
                (#{"home"} tab_button: {TabButtonProps { id: "home".to_owned(), label: "Home".to_owned() } })
                (#{"images"} tab_button: {TabButtonProps { id: "images".to_owned(), label: "Images".to_owned() } })
            ])
        }
    }
}