use raui_core::prelude::*;
use serde::{Deserialize, Serialize};

use crate::components::tab_button_state::tab_button_state;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct TabButtonProps {
    #[serde(default)]
    pub id: String,
    #[serde(default)]
    pub label: String,
}
implement_props_data!(TabButtonProps, "TabButtonProps");

widget_component! {
    pub tab_button(key, props) {
        widget! {
            (#{key} button: {props.clone()} {
                content = (tab_button_state: {props.clone()})
            })
        }
    }
}