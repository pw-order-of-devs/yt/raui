use raui_core::prelude::*;

use crate::components::tab_button::TabButtonProps;

widget_component! {
    pub tab_button_state(key, props) {
        let ButtonProps {
            selected,
            trigger,
            context,
            ..
        } = props.read_cloned_or_default();
        let TabButtonProps {
            id,
            label,
        } = props.read_cloned_or_default();
        let text_props = TextBoxProps {
            text: label,
            direction: TextBoxDirection::HorizontalLeftToRight,
            width: TextBoxSizeValue::Exact(164.0),
            height: TextBoxSizeValue::Exact(32.0),
            alignment: TextBoxAlignment::Center,
            font: TextBoxFont {
                name: "verdana".to_owned(),
                size: 32.0,
                ..Default::default()
            },
            color:
                if trigger { Color { r: 0.0, g: 0.0, b: 0.8, a: 1.0 } }
                else if context { Color { r: 0.8, g: 0.0, b: 0.0, a: 1.0 } }
                else if selected { Color { r: 0.8, g: 0.8, b: 0.8, a: 1.0 } }
                else { Color { r: 0.0, g: 0.0, b: 0.0, a: 1.0 } },
            ..Default::default()
        };

        widget! {
            (#{key} text_box: {text_props})
        }
    }
}
