use raui_core::prelude::*;

use crate::components::{buttons::buttons, images::images, title_bar::title_bar};

widget_component! {
    pub app(key) {
        widget!{
            (#{key} vertical_box: {VerticalBoxProps {
                separation: 16.0,
                ..Default::default()
            }} [
                (title_bar)
                (buttons)
                (images)
            ])
        }
    }
}

