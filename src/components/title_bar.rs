use raui_core::prelude::*;

widget_component! {
    pub title_bar(key) {
        widget! {
            (#{key} text_box: {TextBoxProps {
                text: "RAUI/GGEZ example".to_owned(),
                width: TextBoxSizeValue::Fill,
                height: TextBoxSizeValue::Exact(32.0),
                alignment: TextBoxAlignment::Center,
                font: TextBoxFont {
                    name: "verdana".to_owned(),
                    size: 32.0,
                    ..Default::default()
                },
                color: Color { a: 1.0, r: 0.0, g: 0.0, b: 0.0, },
                ..Default::default()
            }})
        }
    }
}