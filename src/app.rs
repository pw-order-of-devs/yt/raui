use ggez::{Context, event::EventHandler, GameResult, graphics};
use ggez::event::MouseButton;
use raui_core::{application::Application, prelude::*};
use raui_ggez_renderer::prelude::*;

use crate::components::app::app;
use ggez::input::keyboard::{KeyCode, KeyMods};

pub struct App {
    ui: Application,
    ui_interactions: GgezInteractionsEngine,
    ui_resources: GgezResources,
}

impl App {
    pub fn new(ctx: &mut Context) -> Self {
        let mut ui_resources = GgezResources::default();
        ui_resources.fonts.insert(
            "verdana".to_owned(),
            graphics::Font::new(ctx, "/verdana.ttf").expect("GGEZ could not load `verdana.ttf`!"),
        );
        ui_resources.images.insert(
            "back".to_owned(),
            graphics::Image::new(ctx, "/back.png").expect("GGEZ could not load `back.png`!"),
        );
        let ui_interactions = GgezInteractionsEngine::with_capacity(32, 1024);
        let mut ui = Application::new();
        ui.setup(install_components);
        ui.apply(widget! { (app) });
        Self {
            ui,
            ui_interactions,
            ui_resources,
        }
    }
}

impl EventHandler for App {

    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.ui_interactions.update(ctx);
        self.ui.process();
        self.ui
            .interact(&mut self.ui_interactions)
            .expect("Could not interact with UI");
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let (width, height) = graphics::drawable_size(ctx);
        drop(graphics::set_screen_coordinates(
            ctx,
            graphics::Rect::new(0.0, 0.0, width, height),
        ));
        graphics::clear(ctx, graphics::WHITE);
        let ui_space = Rect {
            left: 0.0,
            right: width,
            top: 0.0,
            bottom: height,
        };
        self.ui
            .layout(ui_space, &mut DefaultLayoutEngine)
            .expect("UI could not layout widgets");
        self.ui
            .render(&mut GgezRenderer::new(ctx, &mut self.ui_resources))
            .expect("GGEZ renderer could not render UI");
        graphics::present(ctx)
    }

    fn mouse_button_up_event(&mut self, _ctx: &mut Context, button: MouseButton, x: f32, y: f32,) {
        if button == MouseButton::Left { println!("x: {:?}, y: {:?}", x, y) }
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: KeyCode, _: KeyMods, _: bool) {
        if keycode == KeyCode::Escape { ggez::event::quit(ctx); }
        else { println!("pressed {:?}", keycode) }
        self.ui_interactions.key_down_event(keycode);
    }

    fn focus_event(&mut self, _ctx: &mut Context, gained: bool) {
        if gained { println!("Focused") }
        else { println!("Unfocused") }
    }

    fn quit_event(&mut self, _ctx: &mut Context) -> bool {
        println!("quit_event() callback called, quitting...");
        false
    }

    fn resize_event(&mut self, _ctx: &mut Context, width: f32, height: f32) {
        println!("resizing: w: {:?}, h: {:?}", width, height)
    }
}
