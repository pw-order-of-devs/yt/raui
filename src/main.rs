extern crate serde;
#[macro_use] extern crate raui_core;
extern crate ggez;

mod app;
mod components;

use ggez::{event, event::EventsLoop, ContextBuilder, Context};
use std::path::PathBuf;

use crate::app::App;

fn main() {
    let (mut ctx, mut event_loop) = app_context();
    let mut app = App::new(&mut ctx);
    if let Err(error) = event::run(&mut ctx, &mut event_loop, &mut app) {
        println!("Error: {}", error);
    }
}

fn app_context() -> (Context, EventsLoop) {
    ContextBuilder::new("Sample RAUI/GGEZ App", "PW-OOD")
        .add_resource_path(resource_dir())
        .window_mode(ggez::conf::WindowMode::default().resizable(true).maximized(false),)
        .build()
        .expect("Could not create GGEZ context")
}

fn resource_dir() -> PathBuf {
    if let Ok(manifest_dir) = std::env::var("CARGO_MANIFEST_DIR") {
        let mut path = std::path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        std::path::PathBuf::from("./resources")
    }
}
